/*!
 * @copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2015
 * @version 1.4.1
 *
 * Bootstrap Popover Extended - Popover with modal behavior, styling enhancements and more.
 *
 * For more JQuery/Bootstrap plugins and demos visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */
! function(t) {
  "use strict";
  var e = function(e, o) {
      var r = this;
      r.options = o, r.$element = t(e), r.$dialog = r.$element, r.init()
    },
    o = function(t, e) {
      t.removeClass(e).addClass(e)
    };
  e.prototype = t.extend({}, t.fn.modal.Constructor.prototype, {
    constructor: e,
    init: function() {
      var e = this,
        r = e.$element;
      o(r, "popover-x"), e.$body = t(document.body), e.$target = e.options.$target, e.useOffsetForPos = void 0 === e.options.useOffsetForPos ? !1 : e.options.useOffsetForPos, r.find(".popover-footer").length && o(r, "has-footer"), e.options.remote && r.find(".popover-content").load(e.options.remote, function() {
        r.trigger("load.complete.popoverX")
      }), r.on("click.dismiss.popoverX", '[data-dismiss="popover-x"]', t.proxy(e.hide, e)), r.on("shown.bs.modal", function() {
        e.options.closeOtherPopovers && (r.removeClass("popover-x"), t(".popover-x").each(function() {
          t(this).popoverX("hide")
        }), o(r, "popover-x"))
      })
    },
    getPosition: function() {
      var e = this,
        o = e.$target,
        r = e.useOffsetForPos ? o.offset() : o.position();
      return t.extend({}, r, {
        width: o[0].offsetWidth,
        height: o[0].offsetHeight
      })
    },
    refreshPosition: function() {
      var t, e = this,
        r = e.$element,
        p = e.options.placement,
        a = r[0].offsetWidth,
        i = r[0].offsetHeight,
        s = e.getPosition();
      switch (p) {
        case "bottom":
          t = {
            top: s.top + s.height,
            left: s.left + s.width / 2 - a / 2
          };
          break;
        case "bottom bottom-left":
          t = {
            top: s.top + s.height,
            left: s.left
          };
          break;
        case "bottom bottom-right":
          t = {
            top: s.top + s.height,
            left: s.left + s.width - a
          };
          break;
        case "top":
          t = {
            top: s.top - i,
            left: s.left + s.width / 2 - a / 2
          };
          break;
        case "top top-left":
          t = {
            top: s.top - i,
            left: s.left
          };
          break;
        case "top top-right":
          t = {
            top: s.top - i,
            left: s.left + s.width - a
          };
          break;
        case "left":
          t = {
            top: s.top + s.height / 2 - i / 2,
            left: s.left - a
          };
          break;
        case "left left-top":
          t = {
            top: s.top,
            left: s.left - a
          };
          break;
        case "left left-bottom":
          t = {
            top: s.top + s.height - i,
            left: s.left - a
          };
          break;
        case "right":
          t = {
            top: s.top + s.height / 2 - i / 2,
            left: s.left + s.width
          };
          break;
        case "right right-top":
          t = {
            top: s.top,
            left: s.left + s.width
          };
          break;
        case "right right-bottom":
          t = {
            top: s.top + s.height - i,
            left: s.left + s.width
          };
          break;
        default:
          throw "Invalid popover placement '" + p + "'."
      }
      r.css(t), o(r, p + " in")
    },
    show: function() {
      var e = this,
        o = e.$element;
      o.css({
        top: 0,
        left: 0,
        display: "block",
        "z-index": 1050
      }), e.refreshPosition(), t.fn.modal.Constructor.prototype.show.call(e, arguments), o.css({
        padding: 0
      })
    }
  }), t.fn.popoverX = function(o) {
    var r = this;
    return r.each(function() {
      var r = t(this),
        p = r.data("popover-x"),
        a = t.extend({}, t.fn.popoverX.defaults, r.data(), "object" == typeof o && o);
      a.$target || (a.$target = p && p.$target ? p.$target : o.$target || t(o.target)), p || r.data("popover-x", p = new e(this, a)), "string" == typeof o && p[o]()
    })
  }, t.fn.popoverX.defaults = t.extend({}, t.fn.modal.defaults, {
    placement: "right",
    keyboard: !0,
    closeOtherPopovers: !0
  }), t.fn.popoverX.Constructor = e, t(document).ready(function() {
    t(document).on("click", "[data-toggle='popover-x']", function(e) {
      var o = t(this),
        r = o.attr("href"),
        p = t(o.attr("data-target") || r && r.replace(/.*(?=#[^\s]+$)/, "")),
        a = p.data("popover-x") ? "toggle" : t.extend({
          remote: !/#/.test(r) && r
        }, p.data(), o.data());
      e.preventDefault(), p.trigger("click.target.popoverX"), "toggle" !== a ? (a.$target = o, p.popoverX(a).popoverX("show").on("hide", function() {
        o.focus()
      })) : p.popoverX(a).on("hide", function() {
        o.focus()
      })
    }), t(document).on("keyup", "[data-toggle='popover-x']", function(e) {
      var o = t(this),
        r = o.attr("href"),
        p = t(o.attr("data-target") || r && r.replace(/.*(?=#[^\s]+$)/, ""));
      p && 27 === e.which && (p.trigger("keyup.target.popoverX"), p.popoverX("hide"))
    })
  })
}(window.jQuery);
