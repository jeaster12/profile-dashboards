﻿(function($, d3, _) {
    function pivotChart(chartSelector, opts) {
        //return object
        var pivot;

        //constants
        var barGap = .1; //from 1 (100%) to 0 (0%)
        var rectGap = 1; //in pixels
        var axisGap = 40;
        var labelMargin = {
            right: 5,
            bottom: 2
        };

        var pivotMargin = {
            left: 0,
            top: 0,
            right: 40,
            bottom: 40
        };

        var defaultOptions = {
            animationTime: 800,
            showAxes: true,
            buyClass: "buy",
            sellClass: "sell",
            noChangeClass: "no-change",
            meClass: "me",
            peerClass: "peer",
            buyKey: 3,
            sellKey: 1,
            noChangeKey: 2,
            yAxisTicks: 5
        };

        var maskId = "pivot-clipper";
        var labelFilterId = "label-shadow-filter";

        //chart variables
        var chart = d3.select(chartSelector);
        var chartBounds = getChartBounds();

        var svg = createSvg(chart, chartBounds);
        var defs = svg.append("defs");

        var axisContainer = createContainer(svg, "pivot-axis");
        var maskContainer = createContainer(svg, "pivot-mask");
        var zoomContainer = createContainer(maskContainer, "pivot-zoom");
        var chartContaner = createContainer(zoomContainer, "pivot-chart");
        var barsContainer = createContainer(chartContaner, "pivot-bars");
        var labelContainer = createContainer(chartContaner, "pivot-labels");
        var zoom = createZoom();

        var xAxisElement = createContainer(axisContainer);
        var yAxisElement = createContainer(axisContainer);

        //current chart variables
        var barParams;
        var globalXScale;
        var xScale;
        var yScale;
        var opacityScale;

        var redrawAxesFunc;
        //current chart variables end

        var options = _.merge(defaultOptions, opts);

        var isZooming;
        var log = console.log.bind(console);

        function createSvg(element, bounds) {
            return setSize(element.append("svg"), bounds);
        }

        function createContainer(element, id) {
            var elem = element.append("g");
            if (id) {
                elem.attr("id", id);
                elem.attr("class", id);
            }
            return elem;
        }

        function createMask(id) {
            var chartSize = getSize();

            var mask = defs
                .append("mask")
                .attr("id", id);

            setSize(mask, chartSize);

            var maskRect = mask
                .append("rect")
                .attr("fill", "white");

            setSize(maskRect, chartSize);

            return mask;
        }

        function createLabelFilters(id) {
            var filter = defs.append("filter")
                .attr("id", id)
                .attr("x", "-50%")
                .attr("y", "-50%")
                .attr("height", "200%")
                .attr("width", "200%");

            filter.append("feOffset")
                .attr("dx", 0)
                .attr("dy", 0)
                .attr("result", "offOut");

            filter.append("feFlood")
                .attr("flood-color", "white");

            filter.append("feComposite")
                .attr("operator", "in")
                .attr("in2", "offOut");


            // SourceAlpha refers to opacity of graphic that this filter will be applied to
            // convolve that with a Gaussian with standard deviation 3 and store result
            // in blur
            filter.append("feGaussianBlur")
                .attr("stdDeviation", 4)
                .attr("result", "glow");

            // translate output of Gaussian blur to the right and downwards with 2px
            // store result in offsetBlur


            // overlay original SourceGraphic over translated blurred opacity by using
            // feMerge filter. Order of specifying inputs is important!
            var feMerge = filter.append("feMerge");

            feMerge.append("feMergeNode");
            feMerge.append("feMergeNode");

            feMerge.append("feMergeNode")
                .attr("in", "SourceGraphic");
        }

        function createZoom() {
            function getPoint(event) {
                return _.at(event, ["sourceEvent.x", "sourceEvent.y"]);
            }

            var zoomStartEvent;

            return d3.behavior.zoom()
                .translate([0, 0])
                .scale(1)
                .scaleExtent([1, 8])
                .on("zoom", onZoomInternal)
                .on("zoomstart", function() {
                    zoomStartEvent = d3.event;
                    isZooming = true;
                }).on("zoomend", function() {
                    var initialPoint = getPoint(zoomStartEvent);
                    var finishPoint = getPoint(d3.event);

                    isZooming = !_.isEqual(initialPoint, finishPoint);

                    setTimeout(function() {
                        isZooming = false;
                    });
                });
        }

        function attachEvents() {
            svg
                //attach zoom
                .call(zoom)
                //remove zoom on dblclick
                .on("dblclick.zoom", null);
        }

        function getBarParams(width, height, count) {
            var total = height * width / count;
            var size = Math.sqrt(total);

            var countY = Math.ceil(height / size);
            var countX = Math.ceil(width / size);

            var rectHeight = height / countY;
            var rectWidth = width / countX;
            var rectSize = Math.min(rectWidth, rectHeight);

            return {
                countX: countX,
                countY: countY,
                width: rectSize * countX,
                height: rectSize * countY,
                size: rectSize
            }
        }

        function getTypeClass(type) {
            switch (type) {
            case options.sellKey:
                return options.sellClass;
            case options.noChangeKey:
                return options.noChangeClass;
            case options.buyKey:
                return options.buyClass;
            default:
                return "";
            }
        }

        function configure() {
            if (!options.showAxes) {
                pivotMargin.bottom = 0;
                pivotMargin.right = 0;
            }

            createMask(maskId);
            createLabelFilters(labelFilterId);
            attachEvents();

            maskContainer.attr("mask", "url(#" + maskId + ")");
        }

        function getChartBounds() {
            var chart = $(chartSelector);
            return {
                width: chart.width(),
                height: chart.height()
            };
        }

        function translate(x, y) {
            x = x || 0;
            y = y || 0;

            return "translate(" + x + "," + y + ")";
        }

        function getSize() {
            return {
                width: chartBounds.width - pivotMargin.left - pivotMargin.right,
                height: chartBounds.height - pivotMargin.top - pivotMargin.bottom
            };
        }


        function setSize(element, bounds) {
            return element
                .attr("width", bounds.width)
                .attr("height", bounds.height);
        }

        function resetZoom() {
            onZoomInternal({
                scale: 1,
                translate: [0, 0]
            });
        }

        function onZoomInternal(event) {
            var e = event || d3.event;
            // constrain the x and y components of the translation by the
            // dimensions of the viewport
            var tx = Math.min(0, Math.max(e.translate[0], chartBounds.width - chartBounds.width * e.scale));
            var ty = Math.min(0, Math.max(e.translate[1], chartBounds.height - chartBounds.height * e.scale));
            var translate = [tx, ty];
            // then, update the zoom behavior's internal translation, so that
            // it knows how to properly manipulate it on the next movement
            zoom.translate(translate);

            // correct translation and scale (in reverse order)
            zoomContainer.attr("transform", "translate(" + [tx, ty] + ") scale(" + e.scale + ")");

            if (redrawAxesFunc) {
                redrawAxesFunc({
                    translate: translate,
                    scale: e.scale
                });
            }
        }

        function getRandomCoordinate(data, index) {
            return Math.random() * index * -Math.random() * index;
        }

        function getXCoordinate(itemIndex, barIndex) {
            var row = itemIndex % barParams.countX;
            return xScale(row) + globalXScale(barIndex);
        }

        function getYCoordinate(index) {
            var row = Math.floor(index / barParams.countX);
            return yScale(row);
        }

        function drawTickers(data) {
            data = _
                .chain(data)
                .map(function(bar, barIdx) {
                    _.forEach(bar.array, function(item, itemIdx) {
                        item._groupId = bar.id;
                        item._groupIdx = barIdx;
                        item._itemIdx = itemIdx;
                    });
                    return bar.array;
                })
                .flatten(data)
                .value();

            //append data
            var rect = barsContainer
                .selectAll("rect")
                .data(data,
                    function(d) {
                        return d.id;
                    });

            //set random coordinates on create
            rect
                .enter()
                .append("rect")
                .attr("x", getRandomCoordinate)
                .attr("y", getRandomCoordinate);


            //attach click handler
            rect
                .on("click", function(dataArray) {
                    log("pivot click");
                    var handler = options.onGroupClick;
                    if (handler && !isZooming) {
                        handler.call(pivot, dataArray);
                    }
                });

            //set attributes
            rect
                .attr("class", function(d) {
                    var classes = ["rect", getTypeClass(d.type)];
                    if (d.isMe) {
                        classes.push(options.meClass);
                    }
                    if (d.isPeer) {
                        classes.push(options.peerClass);
                    }

                    return classes.join(" ");
                })
                .attr("rx", 1)
                .attr("ry", 1);

            rect.append("title").text(function(d) {
                return d.title;
            });

            //update existing with animation
            rect
                .transition()
                .duration(options.animationTime)
                .style("fill-opacity", function(d) {
                    if (d.type === 2) {
                        return 1;
                    }
                    return opacityScale(Math.abs(d.valChange));
                })
                .attr("x", function(d) {
                    return getXCoordinate(d._itemIdx, d._groupIdx);
                })
                .attr("y", function(d) {
                    return getYCoordinate(d._itemIdx);
                })
                .attr("width", xScale.rangeBand() - rectGap)
                .attr("height", yScale.rangeBand() - rectGap);

            //animate removal
            rect
                .exit()
                .transition()
                .duration(options.animationTime)
                .attr("x", getRandomCoordinate)
                .attr("y", getRandomCoordinate)
                .style("opacity", function() {
                    return 0;
                })
                .remove();

            return rect;
        }

        function drawAxes(data) {
            var outerWidth = chartBounds.width;

            var chartSize = getSize();

            function transformYAxis(g) {
                g.selectAll("text")
                    .attr("x", -4)
                    .attr("dy", 4);
            }

            var maxChartCount = chartSize.height / yScale.rangeBand() * barParams.countX;

            var titles = _.map(data, "name");

            var xAxisScale = d3.scale.ordinal()
                .domain(titles);

            var yAxisScale = d3.scale.linear()
                .domain([0, maxChartCount])
                .range([chartSize.height, 0]);

            var xAxis = d3.svg.axis()
                .scale(xAxisScale)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(yAxisScale)
                .tickSize(outerWidth)
                .ticks(options.yAxisTicks)
                .orient("left");

            zoom.x(yAxisScale);
            zoom.y(yAxisScale);


            xAxisElement
                .attr("class", "x axis")
                .attr("transform", translate(0, chartSize.height));

            yAxisElement
                .attr("class", "y axis")
                .attr("transform", translate(outerWidth));

            redrawAxesFunc = function(event) {
                var scale = event ? event.scale : 1;
                var translateX = event ? event.translate[0] : 0;

                xAxisScale
                    .rangeBands([0, chartSize.width * scale], barGap * scale);

                xAxisElement
                    .attr("transform", translate(translateX, chartSize.height))
                    .call(xAxis);

                yAxisElement
                    .call(yAxis)
                    .call(transformYAxis)
                    .selectAll("line")
                    .attr("x1", -axisGap);
            };

            redrawAxesFunc();

            return axisContainer;
        }

        function drawLabels(data) {
            var barWidth = globalXScale.rangeBand();

            var labelBars = labelContainer.selectAll("g")
                .data(data, function(data) {
                    return data.id;
                });

            //animate entering
            labelBars
                .enter()
                .append("g");

            //delete old labels on exit
            labelBars
                .exit()
                .remove();

            labelBars.attr("transform", function(d, i) {
                return translate(globalXScale(i));
            });

            var labelContainers =
                labelBars.selectAll("g")
                    .data(function(d) {
                        var value = _
                            .chain(d.array)
                            .groupBy(function(item) {
                                return item.type;
                            })
                            .map(function(array, key) {
                                return {
                                    key: +key,
                                    count: array.length
                                };
                            })
                            .value();
                        var increment = 0;

                        _.forEach(value, function(val) {
                            val.total = increment;
                            increment += val.count;
                        });

                        value.unshift({
                            key: "total",
                            count: d.array.length,
                            total: d.array.length
                        });

                        return value;
                    });

            var text = labelContainers
                .enter()
                .append("g")
                .attr("class", "label-container")
                .append("text")
                .attr("class", function(d) {
                    return "bar-text " + getTypeClass(d.key);
                })
                .style("text-anchor", "end")
                .attr("x", barWidth - labelMargin.right);

            text.attr("filter", "url(#" + labelFilterId + ")");

            text
                .append("tspan")
                .attr("class", "bar-icon")
                .text(function(d) {
                    switch (d.key) {
                    case options.sellKey:
                        return "\uf063";
                    case options.buyKey:
                        return "\uf062";
                    default:
                        return "";
                    }
                });

            text
                .append("tspan")
                .attr("dx", function(d) {
                    switch (d.key) {
                    case options.sellKey:
                    case options.buyKey:
                        return 5;
                    default:
                        return 0;
                    }
                })
                .text(function(d) {
                    return d.count;
                });

            labelContainers.attr("transform", function(d) {
                //todo fix coordinates
                return translate(0, Math.max(10, getYCoordinate(d.total) - labelMargin.bottom));
            });
        }

        function draw(data) {
            if (!_.isArray(data)) {
                data = [data];
            }
            var biggestGroup = _.maxBy(data, "array.length");
            var biggestGroupCount = _.get(biggestGroup, "array.length");

            var opacityExtent = [0, 100];
            if (biggestGroup) {
                opacityExtent = d3.extent(biggestGroup.array, function(item) {
                    return Math.abs(item.valChange);
                });
            }

            var chartSize = getSize();

            globalXScale = d3.scale.ordinal()
                .domain(d3.range(0, data.length))
                .rangeBands([0, chartSize.width], barGap);

            var barWidth = globalXScale.rangeBand();

            barParams = getBarParams(barWidth, chartSize.height, biggestGroupCount);

            xScale = d3.scale.ordinal()
                .domain(d3.range(0, barParams.countX))
                .rangeBands([0, barWidth]);

            yScale = d3.scale.ordinal()
                .domain(d3.range(0, barParams.countY))
                .rangeBands([barParams.height, 0]);

            opacityScale = d3.scale.sqrt()
                .exponent(0.1)
                .domain(opacityExtent)
                .range([0.3, 1]);

            //chartContaner.attr("transform", translate(0, chartSize.height - barParams.height));

            chartContaner
                .transition()
                .duration(options.animationTime)
                .attr("transform", translate(0, chartSize.height - barParams.height));

            maskContainer
                .transition()
                .duration(options.animationTime)
                .attr("transform", translate(pivotMargin.left, pivotMargin.top));

            if (options.showAxes) {
                drawAxes(data);
            }
            resetZoom();
            drawTickers(data);
            drawLabels(data);
        }

        configure();


        pivot = {
            chartId: chartSelector,
            draw: draw
        };
        return pivot;
    }

    window.PivotChart = pivotChart;

})(jQuery, d3, _);