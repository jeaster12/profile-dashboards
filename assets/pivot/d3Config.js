(function(_, d3) {
    //EDIT THIS TO CHANGE TOTAL AMOUNT OF SHOWN ITEMS
    var totalAmount = 2000;

    //EDIT THIS FUNC TO SHOW FAKE TICKER TABLE VIEW
    function showFakeTable() {
				$('#content').toggleClass('content-active');
				$('#button').toggleClass('button-active');
				return false;
				}

    var entireSet;
    var currentGrouping;
    var macroIndustryFilters = [];
    var midIndustryFilters = [];
    var microIndustryFilters = [];
    var filters = [macroIndustryFilters, midIndustryFilters, microIndustryFilters];

    var macroFilterElement = $("#industry-macro-filter");
    var midFilterElement = $("#industry-mid-filter");
    var microFilterElement = $("#industry-micro-filter");

    var isFullMicroView = false;

    var options = {
        onGroupClick: function(data) {
            filterIndustryTickers(data);
        }
    };

    var pivot = new PivotChart("#chart", options);

    function orderData(data) {
        return _.orderBy(data, ["valChange", "val"], ["asc", "desc"]);
    }

    function getFilterApi(element) {
        return _.get(element, "[0].selectize");
    }

    function toggleFilter(groupingToSet, element, array, val) {
        currentGrouping = groupingToSet;

        var filterApi = getFilterApi(element);

        //clear ui filter
        var toRemove = _.without(filterApi.getValue(), val);
        _.forEach(toRemove, function(removeItem) {
            filterApi.removeItem(removeItem);
        });
        filterApi.addItem(val);
        redraw();
    }

    function filterIndustryTickers(data) {
        var groupId = data._groupId;
        if (isFullMicroView && currentGrouping === "micro") {
            showFakeTable();
            return;
        }
        switch (currentGrouping) {
        case "macro":
            toggleFilter("mid", macroFilterElement, macroIndustryFilters, groupId);
            break;
        case "mid":
            toggleFilter("micro", midFilterElement, midIndustryFilters, groupId);
            break;
        case "micro":
            toggleFilter("micro", microFilterElement, microIndustryFilters, groupId);
            isFullMicroView = true;
            break;
        }
    }

    function has(object, data) {
        return _.isEqual(_.pick(object, _.keys(data)), data);
    }

    function redraw() {
        var groupKey = currentGrouping;
        var chained = _.chain(entireSet);

        _.forEach(filters, function(filterSet) {
            chained = chained.filter(function(data) {
                if (filterSet.length === 0) {
                    return true;
                }
                return _.some(filterSet, function(filter) {
                    return has(data, filter);
                });
            });
        });

        if (groupKey) {
            chained = chained
                .groupBy(function(item) {
                    return item[groupKey];
                })
                .map(function(array, key) {
                    if (!key) {
                        return null;
                    }
                    return {
                        id: key,
                        name: key,
                        array: orderData(array)
                    };
                })
                .compact()
                .orderBy("name");
        }

        var dataToDraw = chained
            .value();

        if (!groupKey) {
            dataToDraw =
            {
                id: "All",
                name: "All",
                array: dataToDraw
            };
        }
        pivot.draw(dataToDraw);
    }

    function setGrouping(key) {
        currentGrouping = key;
        redraw();
    }

    function createFiltersData() {
        function groupFilters(data, groupKey, parentGroup) {
            if (!groupKey) {
                return null;
            }

            return _
                .chain(data)
                .groupBy(function(item) {
                    return item[groupKey];
                })
                .map(function(items, key) {
                    var filterItem = {};
                    filterItem[groupKey] = key;
                    return {
                        id: key,
                        parentGroupId: parentGroup,
                        filterItem: filterItem,
                        data: items
                    };
                })
                .filter("id.length")
                .orderBy("id")
                .value();
        }

        function createInner(data, key) {
            _.forEach(data, function(item) {
                item.data = groupFilters(item.data, key, item.id);
                switch (key) {
                case "mid":
                    createInner(item.data, "micro");
                    break;
                case "micro":
                    createInner(item.data);
                    break;
                default:
                    delete item.data;
                    break;
                }
            });
        }

        var macroFilters = groupFilters(entireSet, "macro");
        createInner(macroFilters, "mid");

        return macroFilters;
    }

    $("[name=groups]").click(function() {
        var val = this.value;
        if (val === "") {
            val = null;
        }
        setGrouping(val);
    });

    var debouncedRedraw = _.debounce(redraw, 100, { trailing: true });

    function createFilters() {

        function createFilter(element, childElement, filtersArray, placeholder, data) {
            if (!data) {
                data = [];
            }

            element.selectize({
                plugins: ["remove_button"],
                placeholder: placeholder,
                options: data,
                valueField: "id",
                labelField: "id",
                optgroupValueField: "parentGroupId",
                optgroupLabelField: "parentGroupId",
                optgroupField: "parentGroupId",
                onItemAdd: function(key) {
                    var group = this.options[key];

                    filtersArray.push(group.filterItem);

                    var childFilterApi = getFilterApi(childElement);
                    if (childFilterApi) {
                        childFilterApi.addOptionGroup(group.id, {});
                        childFilterApi.addOption(group.data);
                    }

                    redraw();
                },
                onItemRemove: function(key) {
                    var group = this.options[key];
                    if (!group) {
                        return;
                    }

                    _.remove(filtersArray, group.filterItem);

                    var childFilterApi = getFilterApi(childElement);
                    if (childFilterApi) {
                        _.forEach(group.data, function(item) {
                            childFilterApi.removeItem(item.id);
                            childFilterApi.removeOption(item.id);
                        });
                    }


                    if (filtersArray.length === 0) {
                        if (microIndustryFilters.length === 0 && midIndustryFilters.length === 0) {
                            if (currentGrouping === "micro") {
                                currentGrouping = "mid";
                            }
                            if (macroIndustryFilters.length === 0 && currentGrouping) {
                                currentGrouping = "macro";
                            }
                        }
                    }

                    debouncedRedraw();
                },
                onChange: function() {
                    isFullMicroView = false;
                }
            });
        }

        var filtersData = createFiltersData();
        createFilter(macroFilterElement, midFilterElement, macroIndustryFilters, "Select Macro Industry", filtersData);
        createFilter(midFilterElement, microFilterElement, midIndustryFilters, "Select Mid Industry");
        createFilter(microFilterElement, null, microIndustryFilters, "Select Micro Industry");
    }

    d3.csv("data.csv", function(securities) {
        d3.shuffle(securities);

        entireSet = _
            .chain(securities)
            .map(function(item) {
                var symbol = item["Symbol"];
                if (symbol.length === 0 || symbol === "NULL") {
                    return null;
                }

                var flags = +item["Flags"];

                var valueChange = +item["Value Change ($m)"];
                var type;
                if (valueChange === 0) {
                    type = 2;
                } else if (valueChange < 0) {
                    type = 1;
                } else {
                    type = 3;
                }

                var securityName = item["Security Name"];
                var value = +item["Value ($m)"];
                var exchange = item["Stock Listing: Exchange"];
                var market = item["Market Cap Word"];
                var marketValue = item["Fundamentals: Market Cap ($m)"];
                var macro = item["Macro Industry"];
                var mid = item["Mid Industry"];
                var micro = item["Micro Industry"];
                var region = item["Security: Origin Region"];
                var country = item["Security: Origin Country"];
                return {
                    id: symbol,
                    name: securityName,
                    type: type,
                    isMe: flags === 2,
                    isPeer: flags === 3,
                    val: value,
                    valChange: valueChange,
                    title: "(" + symbol + ")" + securityName + " Val: " + item["Value ($m)"] + "| Chg: " + item["Value Change ($m)"],
                    exchange: exchange,
                    region: region,
                    country: country,
                    market: market,
                    marketValue: marketValue,
                    macro: macro,
                    mid: mid,
                    micro: micro
                }
            })
            .compact()
            .orderBy(["isMe", "isPeer"], ["desc", "desc"])
            .take(totalAmount)
            .value();

        entireSet = orderData(entireSet);

        createFilters();

        redraw();
    });
})(_, d3);
