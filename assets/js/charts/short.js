
      AmCharts.makeChart("chartdiv", {
        "type": "pie",
        "balloonText": "",
        "innerRadius": "70%",
        "labelRadius": 1,
        "labelText": "",
        "radius": 90,
        "startAngle": 180,
        "baseColor": "#9FD356",
        "groupedAlpha": 0,
        "labelsEnabled": false,
        "sequencedAnimation": false,
        "startDuration": 1,
        "startEffect": "easeInSine",
        "titleField": "category",
        "valueField": "column-1",
        "addClassNames": true,
        "autoDisplay": true,
        "fontFamily": "Arial",
        "allLabels": [{
          "align": "center",
          "bold": true,
          "color": "#666",
          "id": "Label-1",
          "size": 15,
          "text": "Target",
          "x": 0,
          "y": 75
        }],
        "balloon": {
          "disableMouseEvents": false,
          "drop": true,
          "fixedPosition": false,
          "offsetY": 1
        },
        "titles": [],
        "dataProvider": [{
          "category": "Target",
          "column-1": "50"
        }, {
          "category": "",
          "column-1": "10"
        }]
      });

      AmCharts.makeChart("chartdiv2", {
        "type": "pie",
        "balloonText": "",
        "labelText": "",
        "pullOutRadius": "0%",
        "baseColor": "#0079BD",
        "groupedColor": "",
        "marginBottom": 1,
        "marginTop": 1,
        "sequencedAnimation": false,
        "startDuration": 0,
        "titleField": "category",
        "valueField": "column-1",
        "creditsPosition": "bottom-right",
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [{
          "category": "New",
          "column-1": 8
        }, {
          "category": "Unchanged",
          "column-1": 6
        }, {
          "category": "Closed",
          "column-1": 2
        }]
      });

			AmCharts.makeChart("piediv",
				{
					"type": "pie",
					"balloonText": "",
					"innerRadius": "75%",
					"labelRadius": 6,
					"labelText": "",
					"pullOutRadius": "10%",
					"radius": 0,
					"startAngle": 165.6,
					"colors": [
						"#B3CFE8",
						"#F2F3F5"
					],
					"labelsEnabled": false,
					"titleField": "category",
					"valueField": "column-1",
					"addClassNames": true,
					"allLabels": [
						{
							"align": "center",
							"bold": true,
							"color": "#B3CFE8",
							"id": "Label-1",
							"size": 14,
							"text": "TARGET",
							"x": "0%",
							"y": "48%"
						}
					],
					"balloon": {
						"adjustBorderColor": false,
						"borderAlpha": 0,
						"borderThickness": 0,
						"color": "#FFFFFF",
						"disableMouseEvents": false,
						"fillAlpha": 0,
						"fixedPosition": false,
						"horizontalPadding": 0,
						"maxWidth": 0,
						"pointerWidth": 0,
						"shadowAlpha": 0,
						"verticalPadding": 3
					},
					"titles": [],
					"dataProvider": [
						{
							"category": "category 1",
							"column-1": "50"
						},
						{
							"category": "category 3",
							"column-1": "10"
						}
					]
				}
			);
