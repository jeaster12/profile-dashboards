
        jQuery(document).ready(function($) {
            $('.my-slider').unslider();
        });



        AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "categoryField": "category",
            "columnSpacing": 0,
            "plotAreaBorderColor": "#FFFFFF",
            "startDuration": 1,
            "addClassNames": true,
            "autoDisplay": true,
            "borderColor": "#FFFFFF",
            "color": "#777777",
            "fontFamily": "Arial",
            "fontSize": 12,
            "categoryAxis": {
                "gridPosition": "start",
                "axisColor": "#777777",
                "axisThickness": 0,
                "gridColor": "#AAB3B3",
                "gridThickness": 0,
                "labelFrequency": 6,
                "tickLength": 0
            },
            "trendLines": [],
            "graphs": [{
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "bulletSize": 0,
                "id": "AmGraph-1",
                "lineColor": "#C0E7FF",
                "lineThickness": 5,
                "markerType": "circle",
                "title": "Me",
                "type": "smoothedLine",
                "valueField": "Me"
            }, {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "bulletColor": "#FBAB8A",
                "fillColors": "",
                "id": "AmGraph-2",
                "lineColor": "#FBAB8A",
                "lineThickness": 5,
                "title": "Peers",
                "type": "smoothedLine",
                "valueField": "Peers"
            }, {
                "id": "AmGraph-4",
                "lineColor": "#2E81F4",
                "lineThickness": 5,
                "markerType": "circle",
                "title": "Industry",
                "type": "smoothedLine",
                "valueField": "Industry"
            }],
            "guides": [],
            "valueAxes": [{
                "axisTitleOffset": 6,
                "id": "ValueAxis-1",
                "position": "right",
                "unit": "%",
                "gridColor": "#777777",
                "title": ""
            }],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "right",
                "autoMargins": false,
                "color": "#777777",
                "fontSize": 12,
                "markerBorderColor": "#FF8000",
                "markerLabelGap": 10,
                "markerSize": 12,
                "markerType": "circle",
                "position": "top",
                "rollOverColor": "#0079BD",
                "useGraphSettings": true,
                "valueWidth": 3
            },
            "titles": [],
            "dataProvider": [{
                "category": "9/30/12",
                "Industry": "1",
                "Me": "4",
                "Peers": "2"
            }, {
                "category": "category 2",
                "Industry": "2",
                "Me": "3",
                "Peers": "4"
            }, {
                "category": "category 3",
                "Industry": "3",
                "Me": "5",
                "Peers": "5"
            }, {
                "category": "category 4",
                "Industry": "3",
                "Me": "7",
                "Peers": "7"
            }, {
                "category": "category 5",
                "Industry": "2",
                "Me": "7",
                "Peers": "6"
            }, {
                "category": "category 6",
                "Industry": "1",
                "Me": "8",
                "Peers": "4"
            }, {
                "category": "6/29/15",
                "Industry": "1",
                "Me": "9",
                "Peers": "3"
            }]
        });

        AmCharts.makeChart("chartdiv3", {
            "type": "serial",
            "categoryField": "category",
            "columnSpacing": 2,
            "columnWidth": 0.85,
            "rotate": true,
            "autoMargins": false,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "marginTop": 0,
            "plotAreaBorderColor": "#FFFFFF",
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "autoDisplay": true,
            "backgroundAlpha": 0.53,
            "borderAlpha": 1,
            "borderColor": "#FFFFFF",
            "color": "#666666",
            "fontFamily": "Arial",
            "fontSize": 12,
            "categoryAxis": {
                "gridPosition": "start",
                "axisColor": "#FFFFFF",
                "axisThickness": 0,
                "gridColor": "#FFFFFF",
                "inside": true,
                "labelOffset": 2
            },
            "trendLines": [],
            "graphs": [{
                "alphaField": "fill alpha",
                "bulletBorderAlpha": 1,
                "bulletBorderColor": "#FF8000",
                "bulletColor": "#FF8000",
                "connect": false,
                "dashLengthField": "dash length",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "labelPosition": "inside",
                "lineColor": "#CDECFF",
                "title": "graph 1",
                "topRadius": 0,
                "type": "column",
                "valueField": "column-1",
                "visibleInLegend": false
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "radarCategoriesEnabled": false,
                "stackType": "regular",
                "type": "number",
                "zeroGridAlpha": 0,
                "axisAlpha": 0.97,
                "axisColor": "#fff",
                "dashLength": 0,
                "fillAlpha": 1,
                "gridColor": "#FFFFFF",
                "gridCount": 0,
                "gridThickness": 0,
                "minHorizontalGap": 65,
                "minorGridAlpha": 1,
                "minVerticalGap": 15,
                "tickLength": 2,
                "title": "",
                "titleBold": false
            }],
            "allLabels": [],
            "balloon": {
                "color": "#787878"
            },
            "titles": [{
                "id": "",
                "text": ""
            }],
            "dataProvider": [{
                "category": "Fomento Economico Mexicano, S.A.B. de C.V. ADR",
                "column-1": 8,
                "column-2": 15
            }, {
                "category": "BHP Billiton Limited",
                "column-1": 16,
                "column-2": 12
            }, {
                "category": "Banco Bradesco S.A. ADR",
                "column-1": 2,
                "column-2": 17
            }, {
                "category": "Tenaris S.A. ADR",
                "column-1": 7,
                "column-2": 11
            }, {
                "category": "Ayala Land, Inc.",
                "column-1": 5,
                "column-2": 7
            }]
        });


        AmCharts.makeChart("chartdivgreen", {
            "type": "serial",
            "categoryField": "category",
            "columnSpacing": 2,
            "columnWidth": 0.85,
            "rotate": true,
            "autoMargins": false,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "marginTop": 0,
            "plotAreaBorderColor": "#FFFFFF",
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "autoDisplay": true,
            "backgroundAlpha": 0.53,
            "borderAlpha": 1,
            "borderColor": "#FFFFFF",
            "color": "#666666",
            "fontFamily": "Arial",
            "fontSize": 12,
            "categoryAxis": {
                "gridPosition": "start",
                "axisColor": "#FFFFFF",
                "axisThickness": 0,
                "gridColor": "#FFFFFF",
                "inside": true,
                "labelOffset": 2
            },
            "trendLines": [],
            "graphs": [{
                "alphaField": "fill alpha",
                "bulletBorderAlpha": 1,
                "bulletBorderColor": "#FF8000",
                "bulletColor": "#FF8000",
                "connect": false,
                "dashLengthField": "dash length",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "labelPosition": "inside",
                "lineColor": "#E1F7D6",
                "title": "graph 1",
                "topRadius": 0,
                "type": "column",
                "valueField": "column-1",
                "visibleInLegend": false
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "radarCategoriesEnabled": false,
                "stackType": "regular",
                "type": "number",
                "zeroGridAlpha": 0,
                "axisAlpha": 0.97,
                "axisColor": "#fff",
                "dashLength": 0,
                "fillAlpha": 1,
                "gridColor": "#FFFFFF",
                "gridCount": 0,
                "gridThickness": 0,
                "minHorizontalGap": 65,
                "minorGridAlpha": 1,
                "minVerticalGap": 15,
                "tickLength": 2,
                "title": "",
                "titleBold": false
            }],
            "allLabels": [],
            "balloon": {
                "color": "#787878"
            },
            "titles": [{
                "id": "",
                "text": ""
            }],
            "dataProvider": [{
                "category": "Fomento Economico Mexicano, S.A.B. de C.V. ADR",
                "column-1": 8,
                "column-2": 15
            }, {
                "category": "BHP Billiton Limited",
                "column-1": 16,
                "column-2": 12
            }, {
                "category": "Banco Bradesco S.A. ADR",
                "column-1": 2,
                "column-2": 17
            }, {
                "category": "Tenaris S.A. ADR",
                "column-1": 7,
                "column-2": 11
            }, {
                "category": "Ayala Land, Inc.",
                "column-1": 5,
                "column-2": 7
            }]
        });



        AmCharts.makeChart("chartdivred", {
            "type": "serial",
            "categoryField": "category",
            "columnSpacing": 2,
            "columnWidth": 0.85,
            "rotate": true,
            "autoMargins": false,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "marginTop": 0,
            "plotAreaBorderColor": "#FFFFFF",
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "autoDisplay": true,
            "backgroundAlpha": 0.53,
            "borderAlpha": 1,
            "borderColor": "#FFFFFF",
            "color": "#666666",
            "fontFamily": "Arial",
            "fontSize": 12,
            "categoryAxis": {
                "gridPosition": "start",
                "axisColor": "#FFFFFF",
                "axisThickness": 0,
                "gridColor": "#FFFFFF",
                "inside": true,
                "labelOffset": 2
            },
            "trendLines": [],
            "graphs": [{
                "alphaField": "fill alpha",
                "bulletBorderAlpha": 1,
                "bulletBorderColor": "#FF8000",
                "bulletColor": "#FF8000",
                "connect": false,
                "dashLengthField": "dash length",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "labelPosition": "inside",
                "lineColor": "#FDDBD9",
                "title": "graph 1",
                "topRadius": 0,
                "type": "column",
                "valueField": "column-1",
                "visibleInLegend": false
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "radarCategoriesEnabled": false,
                "stackType": "regular",
                "type": "number",
                "zeroGridAlpha": 0,
                "axisAlpha": 0.97,
                "axisColor": "#fff",
                "dashLength": 0,
                "fillAlpha": 1,
                "gridColor": "#FFFFFF",
                "gridCount": 0,
                "gridThickness": 0,
                "minHorizontalGap": 65,
                "minorGridAlpha": 1,
                "minVerticalGap": 15,
                "tickLength": 2,
                "title": "",
                "titleBold": false
            }],
            "allLabels": [],
            "balloon": {
                "color": "#787878"
            },
            "titles": [{
                "id": "",
                "text": ""
            }],
            "dataProvider": [{
                "category": "Fomento Economico Mexicano, S.A.B. de C.V. ADR",
                "column-1": 8,
                "column-2": 15
            }, {
                "category": "BHP Billiton Limited",
                "column-1": 16,
                "column-2": 12
            }, {
                "category": "Banco Bradesco S.A. ADR",
                "column-1": 2,
                "column-2": 17
            }, {
                "category": "Tenaris S.A. ADR",
                "column-1": 7,
                "column-2": 11
            }, {
                "category": "Ayala Land, Inc.",
                "column-1": 5,
                "column-2": 7
            }]
        });



              AmCharts.makeChart("chartdivpie", {
                "type": "pie",
                "balloonText": "",
                "innerRadius": "70%",
                "labelRadius": 1,
                "labelText": "",
                "radius": 90,
                "startAngle": 180,
                "baseColor": "#9FD356",
                "groupedAlpha": 0,
                "labelsEnabled": false,
                "sequencedAnimation": false,
                "startDuration": 1,
                "startEffect": "easeInSine",
                "titleField": "category",
                "valueField": "column-1",
                "addClassNames": true,
                "autoDisplay": true,
                "fontFamily": "Arial",
                "allLabels": [{
                  "align": "center",
                  "bold": true,
                  "color": "#666",
                  "id": "Label-1",
                  "size": 15,
                  "text": "Target",
                  "x": 0,
                  "y": 75
                }],
                "balloon": {
                  "disableMouseEvents": false,
                  "drop": true,
                  "fixedPosition": false,
                  "offsetY": 1
                },
                "titles": [],
                "dataProvider": [{
                  "category": "Target",
                  "column-1": "50"
                }, {
                  "category": "",
                  "column-1": "10"
                }]
              });
	AmCharts.makeChart("chartbar",
  {
   "type": "serial",
   "categoryField": "category",
   "startDuration": 1,
   "borderColor": "#777777",
   "color": "#777777",
   "fontFamily": "Arial",
   "theme": "default",
   "categoryAxis": {
    "gridPosition": "start",
    "tickPosition": "start",
    "axisColor": "#777777",
    "axisThickness": 3,
    "color": "#777777",
    "gridColor": "#AAB3B3",
    "labelFrequency": 7,
    "tickLength": 0,
    "titleBold": false
   },
   "trendLines": [],
   "graphs": [
    {
     "bulletBorderThickness": 3,
     "bulletColor": "#FF8000",
     "bulletSize": 15,
     "colorField": "color",
     "columnWidth": 0.7,
     "cornerRadiusTop": 3,
     "fillAlphas": 0.62,
     "id": "AmGraph-1",
     "labelAnchor": "middle",
     "labelPosition": "bottom",
     "lineColorField": "color",
     "title": "",
     "type": "column",
     "valueField": "column-1",
     "visibleInLegend": false
    }
   ],
   "guides": [],
   "valueAxes": [
    {
     "id": "ValueAxis-1",
     "position": "right",
     "axisColor": "#FFFFFF",
     "axisThickness": 0,
     "gridColor": "#AAB3B3",
     "gridCount": 2,
     "tickLength": 0,
     "title": ""
    }
   ],
   "allLabels": [],
   "balloon": {
    "drop": true,
    "showBullet": true
   },
   "titles": [
    {
     "id": "Title-1",
     "size": 15,
     "text": ""
    }
   ],
   "dataProvider": [
    {
     "category": "9/30/12",
     "column-1": "0.8",
     "color": "#8EB5DD"
    },
    {
     "category": "category 2",
     "column-1": "0.6",
     "color": "#8EB5DD"
    },
    {
     "category": "category 3",
     "column-1": "0.4",
     "color": "#8EB5DD"
    },
    {
     "category": "category 4",
     "column-1": "0.7",
     "color": "#8EB5DD"
    },
    {
     "category": "category 5",
     "column-1": "0.5",
     "color": "#8EB5DD"
    },
    {
     "category": "category 6",
     "column-1": "0.9",
     "color": "#8EB5DD"
    },
    {
     "category": "category 7",
     "column-1": "0.4",
     "color": "#8EB5DD"
    },
    {
     "category": "6/30/15",
     "color": "#8EB5DD",
     "column-1": "0.5"
    }
   ]
  }
 );



			AmCharts.makeChart("piediv",
				{
					"type": "pie",
					"balloonText": "",
					"innerRadius": "75%",
					"labelRadius": 6,
					"labelText": "",
					"pullOutRadius": "10%",
					"radius": 0,
					"startAngle": 165.6,
					"colors": [
						"#B3CFE8",
						"#F2F3F5"
					],
					"labelsEnabled": false,
					"titleField": "category",
					"valueField": "column-1",
					"addClassNames": true,
					"allLabels": [
						{
							"align": "center",
							"bold": true,
							"color": "#B3CFE8",
							"id": "Label-1",
							"size": 14,
							"text": "TARGET",
							"x": "0%",
							"y": "48%"
						}
					],
					"balloon": {
						"adjustBorderColor": false,
						"borderAlpha": 0,
						"borderThickness": 0,
						"color": "#FFFFFF",
						"disableMouseEvents": false,
						"fillAlpha": 0,
						"fixedPosition": false,
						"horizontalPadding": 0,
						"maxWidth": 0,
						"pointerWidth": 0,
						"shadowAlpha": 0,
						"verticalPadding": 3
					},
					"titles": [],
					"dataProvider": [
						{
							"category": "category 1",
							"column-1": "50"
						},
						{
							"category": "category 3",
							"column-1": "10"
						}
					]
				}
			);


   			AmCharts.makeChart("invest",
   				{
   					"type": "serial",
   					"categoryField": "category",
   					"columnSpacing": 0,
   					"marginLeft": -20,
   					"startDuration": 1,
   					"borderColor": "#FFFFFF",
   					"categoryAxis": {
   						"startOnAxis": true,
   						"tickPosition": "start",
   						"axisColor": "#AAB3B3",
   						"color": "#AAB3B3",
   						"gridAlpha": 0,
   						"gridColor": "#AAB3B3",
   						"gridThickness": 0,
   						"minHorizontalGap": 6,
   						"showLastLabel": false,
   						"tickLength": 1,
   						"title": "",
   						"titleBold": false
   					},
   					"trendLines": [],
   					"graphs": [
   						{
   							"balloonText": "[[title]] of [[category]]:[[value]]",
   							"gapPeriod": 1,
   							"id": "AmGraph-1",
   							"lineColor": "#0072B2",
   							"lineThickness": 3,
   							"title": "Me",
   							"type": "smoothedLine",
   							"valueField": "blue"
   						},
   						{
   							"balloonText": "[[title]] of [[category]]:[[value]]",
   							"bulletBorderThickness": 8,
   							"gapPeriod": 1,
   							"id": "AmGraph-2",
   							"lineColor": "#B27B23",
   							"lineThickness": 3,
   							"title": "Peers",
   							"type": "smoothedLine",
   							"valueField": "orange"
   						},
   						{
   							"balloonColor": "#8960E7",
   							"gapPeriod": 1,
   							"id": "AmGraph-3",
   							"legendAlpha": 0,
   							"legendColor": "#0000FF",
   							"legendValueText": "l;dNAJDN",
   							"lineColor": "#8960E7",
   							"lineThickness": 3,
   							"markerType": "triangleDown",
   							"showBalloon": false,
   							"title": "Industry",
   							"type": "smoothedLine",
   							"valueField": "purple"
   						}
   					],
   					"guides": [],
   					"valueAxes": [
   						{
   							"id": "ValueAxis-1",
   							"minMaxMultiplier": 0,
   							"unit": "%",
   							"axisColor": "#AAB3B3",
   							"axisThickness": 0,
   							"boldLabels": true,
   							"color": "#AAB3B3",
   							"gridColor": "#AAB3B3",
   							"gridCount": 1,
   							"minHorizontalGap": 0,
   							"minVerticalGap": 40,
   							"tickLength": 0,
   							"title": "",
   							"titleBold": false,
   							"titleColor": "#0072B2"
   						}
   					],
   					"allLabels": [],
   					"balloon": {
   						"disableMouseEvents": false
   					},
   					"titles": [],
   					"dataProvider": [
   						{
   							"category": "3/30/15",
   							"blue": "-11",
   							"orange": "-3",
   							"purple": "-10"
   						},
   						{
   							"category": "6/29/15",
   							"blue": "-2",
   							"orange": "-2",
   							"purple": "5"
   						},
   						{
   							"category": "9/29/15",
   							"blue": "-1",
   							"orange": "-2",
   							"purple": "-3"
   						},
   						{
   							"category": "12/29/15",
   							"blue": "12",
   							"orange": "7",
   							"purple": "8"
   						},
   						{
   							"category": "1/1/16",
   							"blue": "13",
   							"orange": "9",
   							"purple": "10"
   						}
   					]
   				}
   			);
